(ns plf02.core)

;; PREDICADOS
;; associative?
(defn función-associative?-1
  [n]
  (associative? n))

(defn función-associative?-2
  [n]
  (associative? n))

(defn función-associative?-3
  [n]
  (associative? n))

(función-associative?-1 [1 2 3])
(función-associative?-2 {["a" "b" "c"] #{1 2 3} '(4 5 6) {}})
(función-associative?-3 '("Hola" '("A" "B" "C") [1 2 3 4] \n))


;; boolean?
(defn función-boolean?-1
  [n]
  (boolean? n))

(defn función-boolean?-2
  [n]
  (boolean? n))

(defn función-boolean?-3
  [n]
  (boolean? n))

(función-boolean?-1 false)
(función-boolean?-2 ['(1 2 3) (< 23 9) 120 "Hola" true ["a" "b" "c"] false])
(función-boolean?-3 '(false (+ 5 10) [1 2 3 4] {"1" "2"} true (> 15 12) #{10 20 30 40}))



;; char?
(defn función-char?-1
  [n]
  (char? n))

(defn función-char?-2
  [n]
  (char? n))

(defn función-char?-3
  [n]
  (char? n))

(función-char?-1 65)
(función-char?-2 \space)
(función-char?-3 '(\g \h))



;; coll?
(defn función-coll?-1
  [n]
  (coll? n))

(defn función-coll?-2
  [n]
  (coll? n))

(defn función-coll?-3
  [n]
  (coll? n))

(función-coll?-1 :nombre)
(función-coll?-2 [1 2 3])
(función-coll?-3 '([1 3 5] #{2 4 6} {10 20 30 40} '(50 60 70) "Si"))



;; decimal?
(defn función-decimal?-1
  [n]
  (decimal? n))

(defn función-decimal?-2
  [n]
  (decimal? n))

(defn función-decimal?-3
  [n]
  (decimal? n))

(función-decimal?-1 0.1M)
(función-decimal?-2 "número")
(función-decimal?-3 12349203)



;; double?
(defn función-double?-1
  [n]
  (double? n))

(defn función-double?-2
  [n]
  (double? n))

(defn función-double?-3
  [n]
  (double? n))

(función-double?-1 \@)
(función-double?-2 12.5)
(función-double?-3 [4 5])



;; float?
(defn función-float?-1
  [n]
  (float? n))

(defn función-float?-2
  [n]
  (float? n))

(defn función-float?-3
  [n]
  (float? n))

(función-float?-1 90.4)
(función-float?-2 #{:uno 1})
(función-float?-3 "flotante")



;; ident?
(defn función-ident?-1
  [n]
  (ident? n))

(defn función-ident?-2
  [n]
  (ident? n))

(defn función-ident?-3
  [n]
  (ident? n))

(función-ident?-1 'a)
(función-ident?-2 :nombre)
(función-ident?-3 {:a 1 :b 2 :c 3})



;; indexed?
(defn función-indexed?-1
  [n]
  (indexed? n))

(defn función-indexed?-2
  [n]
  (indexed? n))

(defn función-indexed?-3
  [n]
  (indexed? n))

(función-indexed?-1 true)
(función-indexed?-2 [1 2 3 4])
(función-indexed?-3 {'(5 6 7 8) [1 3 5 7]})



;; int?
(defn función-int?-1
  [n]
  (int? n))

(defn función-int?-2
  [n]
  (int? n))

(defn función-int?-3
  [n]
  (int? n))

(función-int?-1 20)
(función-int?-2 183.90)
(función-int?-3 [20 25 30 35 40])



;; integer?
(defn función-integer?-1
  [n]
  (integer? n))

(defn función-integer?-2
  [n]
  (integer? n))

(defn función-integer?-3
  [n]
  (integer? n))

(función-integer?-1 34)
(función-integer?-2 18/6)
(función-integer?-3 900.01)



;; keyword?
(defn función-keyword?-1
  [n]
  (keyword? n))

(defn función-keyword?-2
  [n]
  (keyword? n))

(defn función-keyword?-3
  [n]
  (keyword? n))

(función-keyword?-1 'n)
(función-keyword?-2 :->)
(función-keyword?-3 pos?)



;; list?
(defn función-list?-1
  [n]
  (list? n))

(defn función-list?-2
  [n]
  (list? n))

(defn función-list?-3
  [n]
  (list? n))

(función-list?-1 '(1 2 3 4))
(función-list?-2 [(list 1 2 3 4) '(20 40 60)])
(función-list?-3 (list "Lista" \g 120 13.5 [1 2 3] {4 5 6 7}))



;; map-entry?
(defn función-map-entry?-1
  [n]
  (map-entry? n))

(defn función-map-entry?-2
  [n]
  (map-entry? n))

(defn función-map-entry?-3
  [n]
  (map-entry? n))

(función-map-entry?-1 (first {:a 1 :b 2}))
(función-map-entry?-2 (last (hash-map :nombre "Yazmin" :edad 23)))
(función-map-entry?-3 #{:uno 1 :dos 2 :tres 3 :cuatro 4 :cinco 5})



;; map?
(defn función-map?-1
  [n]
  (map? n))

(defn función-map?-2
  [n]
  (map? n))

(defn función-map?-3
  [n]
  (map? n))

(función-map?-1 {:a 1 :b 2 :c 3})
(función-map?-2 [{:uno 1 :dos 2 :tres 3} :a [:1 "Uno"]])
(función-map?-3 (hash-map :diez 10 :veinte 20 :treinta 30))



;; nat-int?
(defn función-nat-int?-1
  [n]
  (nat-int? n))

(defn función-nat-int?-2
  [n]
  (nat-int? n))

(defn función-nat-int?-3
  [n]
  (nat-int? n))

(función-nat-int?-1 43)
(función-nat-int?-2 -89)
(función-nat-int?-3 '(-123 90 45 19 -100))



;; number?
(defn función-number?-1
  [n]
  (number? n))

(defn función-number?-2
  [n]
  (number? n))

(defn función-number?-3
  [n]
  (number? n))

(función-number?-1 72)
(función-number?-2 [172 "A" 12.5 true \n 100])
(función-number?-3 [false "Número" 31 0.65 (+ 23 100)])



;; pos-int?
(defn función-pos-int?-1
  [n]
  (pos-int? n))

(defn función-pos-int?-2
  [n]
  (pos-int? n))

(defn función-pos-int?-3
  [n]
  (pos-int? n))

(función-pos-int?-1 16)
(función-pos-int?-2 -20)
(función-pos-int?-3 132.0)



;; ratio?
(defn función-ratio?-1
  [n]
  (ratio? n))

(defn función-ratio?-2
  [n]
  (ratio? n))

(defn función-ratio?-3
  [n]
  (ratio? n))

(función-ratio?-1 33/8)
(función-ratio?-2 29323)
(función-ratio?-3 2435.95)



;; rational?
(defn función-rational?-1
  [n]
  (rational? n))

(defn función-rational?-2
  [n]
  (rational? n))

(defn función-rational?-3
  [n]
  (rational? n))

(función-rational?-1 50)
(función-rational?-2 89/23)
(función-rational?-3 100.03)



;; seq?
(defn función-seq?-1
  [n]
  (seq? n))

(defn función-seq?-2
  [n]
  (seq? n))

(defn función-seq?-3
  [n]
  (seq? n))

(función-seq?-1 '(1 2 3))
(función-seq?-2 [1 2 '(1 2 3) [4 5 6] {7 8 9 0}])
(función-seq?-3 #{"Prueba" '() (list 1 2 3 4) #{"nombre" "apellido"}})


;; seqable?
(defn función-seqable?-1
  [n]
  (seqable? n))

(defn función-seqable?-2
  [n]
  (seqable? n))

(defn función-seqable?-3
  [n]
  (seqable? n))

(función-seqable?-1 [])
(función-seqable?-2 '("" nil {1 2 3 4} true '("1" "2" "3")))
(función-seqable?-3 #{1.0 40 nil "" false '() {} \v (list 2 4 6 8)})


;; sequencial?
(defn función-sequencial?-1
  [n]
  (sequential? n))

(defn función-sequencial?-2
  [n]
  (sequential? n))

(defn función-sequencial?-3
  [n]
  (sequential? n))

(función-sequencial?-1 nil)
(función-sequencial?-2 {'(0 9 8) 130 "adios" []})
(función-sequencial?-3 [[1 2 3] '("a" "b" "c") 12 "hola" '()])


;; set?
(defn función-set?-1
  [n]
  (set? n))

(defn función-set?-2
  [n]
  (set? n))

(defn función-set?-3
  [n]
  (set? n))

(función-set?-1 #{:1 "a" :2 "b" :3 "c"})
(función-set?-2 {"Alejandra" {1 2 3 4} ["si" "no"] '()})
(función-set?-3 (hash-map ["a" "l" "e"] #{10 20 30} "Yazmin" \b))



;; some?
(defn función-some?-1
  [n]
  (some? n))

(defn función-some?-2
  [n]
  (some? n))

(defn función-some?-3
  [n]
  (some? n))

(función-some?-1 #{})
(función-some?-2 (list true '() {} nil #{}))
(función-some?-3 (hash-set 90 false '(2 4 6 8) #{1 3 5 7}))



;; string?
(defn función-string?-1
  [n]
  (string? n))

(defn función-string?-2
  [n]
  (string? n))

(defn función-string?-3
  [n]
  (string? n))

(función-string?-1 \t)
(función-string?-2 "Hola")
(función-string?-3 'palabra)



;; symbol?
(defn función-symbol?-1
  [x]
  (symbol? x))

(defn función-symbol?-2
  [x]
  (symbol? x))

(defn función-symbol?-3
  [x]
  (symbol? x))

(función-symbol?-1 100)
(función-symbol?-2 \@)
(función-symbol?-3 'k)



;; vector?
(defn función-vector?-1
  [x]
  (vector? x))

(defn función-vector?-2
  [x]
  (vector? x))

(defn función-vector?-3
  [x]
  (vector? x))

(función-vector?-1 '(1 2 3))
(función-vector?-2 [#{1 10 2 20 3 30} (list 2 3 4) '() [10 20 30]])
(función-vector?-3 #{[] (list 1 3 5 7) '(2 4 6 8) (vector 10 20 30 40)})




;;FUNCIONES DE ORDEN SUPERIOR
;; drop
(defn función-drop-1
  [x]
  (drop x))

(defn función-drop-2
  [x y]
  (drop x y))

(defn función-drop-3
  [x y]
  (drop x y))

(función-drop-1 4)
(función-drop-2 2 [1 3 5 7 9 11 13])
(función-drop-3 3 {0 2 4 6 8 10 12 14})



;; drop-last
(defn función-drop-last-1 
  [x]
  (drop-last x))

(defn función-drop-last-2
  [x y]
  (drop-last x y))

(defn función-drop-last-3
  [x y]
  (drop-last x y))

(función-drop-last-1 {"a" "b" "c" "d"})
(función-drop-last-2 4 '(2 8 3 7 4 6 5 1 0))
(función-drop-last-3 5 #{1 9 2 8 3 7 4 6 5})



;; drop-while
(defn función-drop-while-1
  [x]
  (drop-while x))

(defn función-drop-while-2
  [x y]
  (drop-while x y))

(defn función-drop-while-3
  [x y]
  (drop-while x y))

(función-drop-while-1 pos?)
(función-drop-while-2 zero? '(0 3 0 1 3 8 0 3 0))
(función-drop-while-3 number? [4 1 3 6 \n "a" 2 4 '(1 2)])



;; every?
(defn función-every?-1
  [x y]
  (every? x y))

(defn función-every?-2
  [x y]
  (every? x y))

(defn función-every?-3
  [x y]
  (every? x y))

(función-every?-1 decimal? [1 0.9 12 56/6 100 \v "q"])
(función-every?-2 boolean? '(false (< 3 4) (== 1 0) true))
(función-every?-3 char? #{"d" \n 'f '(\h \o \l \a) {"a" "b"}})



;; filterv
(defn función-filterv-1
  [x y]
  (filterv x y))

(defn función-filterv-2
  [x y]
  (filterv x y))

(defn función-filterv-3
  [x y]
  (filterv x y))

(función-filterv-1 neg? '(1 8 -2 0 3 -8 4 3 -1))
(función-filterv-2 boolean? [0 (< 3 7) false (== 7 5) 2 'k])
(función-filterv-3 double? (list (/ 5 2) (* 3 0.3) 0.3 4/2 1M 100))



;; group-by
(defn función-group-by-1
  [x y]
  (group-by x y))

(defn función-group-by-2
  [x y]
  (group-by x y))

(defn función-group-by-3
  [x y]
  (group-by x y))

(función-group-by-1 (fn [y] (/ y 10)) [10 20 30 40 50 60 70 80])
(función-group-by-2 char [65 66 67 68 69 70 71 72 73])
(función-group-by-3 (fn [y] (* y 0.5)) [100 30 80 45 19 921 310 200])



;; iterate
(defn función-iterate-1
  [x y]
  (take 10 (iterate x y)))

(defn función-iterate-2
  [x y]
  (take 5 (iterate x y)))

(defn función-iterate-3
  [x y]
  (take 15 (iterate x y)))

(función-iterate-1 inc 10)
(función-iterate-2 dec 20)
(función-iterate-3 float 90)



;; keep
(defn función-keep-1
  [x]
  (keep x))

(defn función-keep-2
  [x y]
  (keep x y))

(defn función-keep-3
  [x y]
  (keep x y))

(función-keep-1 double)
(función-keep-2 count [[] {} '(1 2 3) #{4 5 6 7} (list "a")])
(función-keep-3 (fn [y] (+ y 100)) [100 200 300 400])



;; keep-indexed
(defn función-keep-indexed-1
  [x]
  (keep-indexed #(if (even? %1) %2 " ") x))

(defn función-keep-indexed-2
  [x]
  (keep-indexed #(if (== 0 (mod %1 3)) %2) x))

(defn función-keep-indexed-3
  [x]
  (keep-indexed #(if (< %1 5) 0 %2) x))

(función-keep-indexed-1 [1 2 3 4 5])
(función-keep-indexed-2 [:1 :a :2 "a" "b"])
(función-keep-indexed-3 "Yazmin Alejandra")



;; map-indexed
(defn función-map-indexed-1
  [x]
  (map-indexed x))

(defn función-map-indexed-2
  [x y]
  (map-indexed x y))

(defn función-map-indexed-3
  [x y]
  (map-indexed x y))

(función-map-indexed-1 "Alejandra")
(función-map-indexed-2 hash-map '(1 2 3 4 5 6 7))
(función-map-indexed-3 vector #{:nombre "Yazmin" :apellido "García"})



;; mapcat
(defn función-mapcat-1
  [x y]
  (mapcat x y)) 

(defn función-mapcat-2
  [x y]
  (mapcat x y))

(defn función-mapcat-3
  [x y]
  (mapcat x y))

(función-mapcat-1 list* [[1 2 3] [4 5 6] [7 8 9]])
(función-mapcat-2 reverse '(("a" "b" "c") ("d" "e" "f") ("g" "h" "i")))
(función-mapcat-3 rest {1 2 3 4 5 6})



;; mapv
(defn función-mapv-1
  [x y]
  (mapv x y))

(defn función-mapv-2
  [x y z]
  (mapv x y z))

(defn función-mapv-3
  [w x y z]
  (mapv w x y z))

(función-mapv-1 dec #{9 8 7 6})
(función-mapv-2 - [10 20 30 40 50 60] [1 2 3 4 5 6])
(función-mapv-3 * '(1 2 3 4 5) '(1 10 100 1000 10000) '(5 4 3 2 1))



;; merge-with
(defn función-merge-with-1
  [x y z]
  (merge-with x y z))

(defn función-merge-with-2
  [w x y z]
  (merge-with w x y z))

(defn función-merge-with-3
  [w x y z]
  (merge-with w x y z))

(función-merge-with-1 - {:a 12 :b 50 :c 29} {:b 53 :c 60 :f 51})
(función-merge-with-2 concat {:1 "a" :2 "b" :3 "c" :4 "d"} {:1 "e" :3 "f" :5 "g"} {:2 "h" :4 "i" :6 "j"})
(función-merge-with-3 max {:a 13 :b 34 :c 2 :d 90} {:a 1 :d 23 :c 1.4} {:b 20 :d 812})



;; not-any?
(defn función-not-any?-1
  [x y]
  (not-any? x y))

(defn función-not-any?-2
  [x y]
  (not-any? x y))

(defn función-not-any?-3
  [x y]
  (not-any? x y))

(función-not-any?-1 number? [\c "número" 120 4.1 true])
(función-not-any?-2 boolean? ['() #{1 2 3} "falso" 150 1.1])
(función-not-any?-3 string? ["hola" \n 123 10 93.2 '() []])



;; not-every?
(defn función-not-every?-1
  [x y]
  (not-every? x y))

(defn función-not-every?-2
  [x y]
  (not-every? x y))

(defn función-not-every?-3
  [x y]
  (not-every? x y))

(función-not-every?-1 float? '(192 91 38 133 -12 32))
(función-not-every?-2 zero? [1 3 0 2 5 2 \b "cero"])
(función-not-every?-3 true? (list (< 9 10) (> 12 9) true))



;; partition-by
(defn función-partition-by-1
  [x y]
  (partition-by x y))

(defn función-partition-by-2
  [x y]
  (partition-by x y))

(defn función-partition-by-3
  [x y]
  (partition-by x y))

(función-partition-by-1 pos? [102 283 -12 -122 816 32 -22])
(función-partition-by-2 double? [100 90 19 -2 1M 23.111 3/7 1])
(función-partition-by-3 float? [1 12.2 4/2 120])



;; reduce-kv

;; remove
(defn función-remove-1
  [x y]
  (remove x y))

(defn función-remove-2
  [x y]
  (remove x y))

(defn función-remove-3
  [x y]
  (remove x y))

(función-remove-1 boolean? [23 \m "123" false (< 3 2) 34])
(función-remove-2 neg? [12 -2 0 3 192 -12 900 1028 -10])
(función-remove-3 float? '(102 -22 102 23/6 10.22 0.1))



;; reverse
(defn función-reverse-1
  [x]
  (reverse x))

(defn función-reverse-2
  [x]
  (reverse x))

(defn función-reverse-3
  [x]
  (reverse x))

(función-reverse-1 ["1" 23 90 1 \a '@ 1.4 [1 2 3]])
(función-reverse-2 '([1 2 3] [\b \c \d \r] ["q" "w" "e" "s"] {0 1 2 3}))
(función-reverse-3 #{'() '(10 20 30 40) {} {0 9 8 7} ["Yazmin" "Alejandra"]})



;; some
(defn función-some-1
  [x y]
  (some x y))

(defn función-some-2
  [x y]
  (some x y))

(defn función-some-3
  [x y]
  (some x y))

(función-some-1 neg? [12 21 82 38 -2 -33])
(función-some-2 coll? [{} '() 39 \p "hola"])
(función-some-3 string? ["Alejandra" 10 20 \c])



;; sort-by
(defn función-sort-by-1
  [x y]
  (sort-by x y))

(defn función-sort-by-2
  [x y]
  (sort-by x y))

(defn función-sort-by-3
  [x y]
  (sort-by x y))

(función-sort-by-1 max [20 38 1 3 27 33 1])
(función-sort-by-2 count '({} [1 2 3 4] '(\b) #{"a" "b" "c"}))
(función-sort-by-3 min [122 1M 0.16 -23 111 0.17 7/20])



;; split-with
(defn función-split-with-1
  [x y]
  (split-with x y))

(defn función-split-with-2
  [x y]
  (split-with x y))

(defn función-split-with-3
  [x y]
  (split-with x y))

(función-split-with-1 string? ["s" "a" \x 18 "manzana" \z])
(función-split-with-2 neg? [-83 -18 0.39 4 -23 -44 20 102 -2])
(función-split-with-3 boolean [0 false true 0 1 false false])



;; take
(defn función-take-1
  [x y]
  (take x y))

(defn función-take-2
  [x y]
  (take x y))

(defn función-take-3
  [x]
  (take x))

(función-take-1 3 [1 2 3 4 5 6 7])
(función-take-2 10 (range 90 102))
(función-take-3 (first '(6 1 3 9 3 2 47 24 2)))



;; take-last
(defn función-take-last-1
  [x y]
  (take-last x y))

(defn función-take-last-2
  [x y]
  (take-last x y))

(defn función-take-last-3
  [x y]
  (take-last x y))

(función-take-last-1 3 #{92 3 4 23 5 12 43 77 1 0 6})
(función-take-last-2 6 '(0 13 45 342 542 654 66 433 23))
(función-take-last-3 7 (range 10))



;; take-nth
(defn función-take-nth-1
  [x y]
  (take-nth x y))

(defn función-take-nth-2
  [x y]
  (take-nth x y))

(defn función-take-nth-3
  [x y]
  (take-nth x y))

(función-take-nth-1 2 [1 2 4 353 3 5 75 3 4])
(función-take-nth-2 3 [3 "si" "no" \v 12.2 {}])
(función-take-nth-3 10 (range 100))



;; take-while
(defn función-take-while-1
  [x y]
  (take-while x y))

(defn función-take-while-2
  [x y]
  (take-while x y))

(defn función-take-while-3
  [x y]
  (take-while x y))

(función-take-while-1 boolean? [false true (< 90 12) \g 2 "d"])
(función-take-while-2 string? ["mientras" \z 'f false '() {1 2 3 4} "toma"])
(función-take-while-3 number? [1 9 23 412 'd \@ "función" {20 3 12 2} [] '()])



;; update
(defn función-update-1
  [x y z]
  (update x y z))

(defn función-update-2
  [x y z]
  (update x y z))

(defn función-update-3
  [x y z]
  (update x y z))

(función-update-1 [1 2 3 4 5 6 7] 6 dec)
(función-update-2 [93 3 12 43 532 9] 3 float)
(función-update-3 [183 2324 232 1413 2314 21] 4 list)



;; update-in
(defn función-update-in-1
  [x y z]
  (update-in x y z))

(defn función-update-in-2
  [x y z]
  (update-in x y z))

(defn función-update-in-3
  [x y z]
  (update-in x y z))

(función-update-in-1 {:a 1 :b 2 :c 3} [:a] float)
(función-update-in-2 {:a 65 :b 66 :c 67 :d 68} [:d] char)
(función-update-in-3 {:nombre "Yazmin" :carrera "ISC"} [:nombre] symbol)